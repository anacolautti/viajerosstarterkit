from datetime import datetime
from filelock import FileLock


class Log:
    def __init__(self):
        self.logfile = "logfile.csv"
        f = open(self.logfile, "a")
        f.close()
        self.generation_logfile = "generation_logfile.csv"
        f = open(self.logfile, "a")
        f.close()

    def write_info(self, location, message):
        date_time_obj = datetime.now()
        timestamp_str = date_time_obj.strftime("%d-%b-%Y--%H-%M-%S")
        lock = FileLock(".lockfile")
        with lock:
            open(self.logfile, "a").write(timestamp_str + ";INFO;"+location+";"+message+"\n")
        lock.release()

    def write_generation_info(self, solution, route_id, visit_index, visit):
        date_time_obj = datetime.now()
        timestamp = date_time_obj.strftime("%d-%b-%Y--%H-%M-%S")
        lock = FileLock(".lockfile")
        with lock:
            open(self.generation_logfile, "a").write(timestamp + ";INFO"
                                                     + ";" + str(solution.generation.id)
                                                     + ";" + str(solution.generation.iteration)
                                                     + ";" + str(solution.generation.run_iteration.current)
                                                     + ";" + str(solution.id)
                                                     + ";" + solution.source
                                                     + ";" + str(solution.cost)
                                                     + ";" + str(route_id)
                                                     + ";" + str(visit_index)
                                                     + ";" + str(visit.id)
                                                     + ";(" + str(visit.horizontal)
                                                     + ", " + str(visit.vertical) + ")"
                                                     + ";" + solution.comments
                                                     + "\n")
        lock.release()