import math
import random
from .helpers import distance, maximum_distance, max_cluster_diameter, \
    create_minimal_clusters, swap_two_locations_in_route, \
    sort_locations_in_routes_by_shared_or_not
import solution.models
import tracerlog
import location.models


class SolutionGenerator:
    def __init__(self, arguments=None):
        if arguments is None:
            arguments = {}
        self.arguments = arguments

        if 'multiplier' not in self.arguments:
            self.arguments.update({'multiplier': 9 / 10})

    def calculate_cost(self, current_solution):
        cost = 0
        start_0 = current_solution.origins.filter(originlocations__routeID=0)
        visits_0 = current_solution.visits.filter(visitlocations__routeID=0).order_by('visitlocations__index')
        previous = start_0[0]
        last = start_0[0]
        for visit in visits_0:
            cost += distance(previous, visit)
            previous = visit
        cost += distance(previous, last)
        start_1 = current_solution.origins.filter(originlocations__routeID=1)
        visits_1 = current_solution.visits.filter(visitlocations__routeID=1).order_by('visitlocations__index')
        previous = start_1[0]
        last = start_1[0]
        for visit in visits_1:
            cost += distance(previous, visit)
            previous = visit
        cost += distance(previous, last)
        if len(visits_0) == 0 or len(visits_1) == 0:
            cost = cost * 2
            logger = tracerlog.Log()
            logger.write_info(self.__class__.__name__,
                              "At CalculateCost(). Penalizing "
                              + str(current_solution.id)
                              + ". Incrementing it's cost by 100% because one of their routes is empty. "
                              )
        visits_in_solution = len(current_solution.visits.all())
        visits_in_general = len(location.models.Location.objects.filter(type="visit"))
        if visits_in_solution < visits_in_general:
            cost = cost * 2
            logger = tracerlog.Log()
            logger.write_info(self.__class__.__name__,
                              "At CalculateCost(). Penalizing "
                              + str(current_solution.id)
                              + ". Incrementing it's cost by 100% because it doesn't visit all the cities. "
                              )
        return cost

    def add_random_route_to_solution(self, generation, origins, visits):
        route1, route2 = [], []
        for visit in visits:
            options = [0, 1]
            if (random.choice(options)) == 0:
                route1.append(visit)
            else:
                route2.append(visit)
        random.shuffle(route1)
        random.shuffle(route2)
        # genera la solucion con los datos obtenidos
        self.create_solution_with_reversed_origins(generation, "RANDOM", origins, route1, route2)

    def add_minimum_distance_route_to_solution(self, generation, origins, visits):
        if generation.id > 10:
            return
        multiplier = generation.id / (generation.id + 1)
        for index in range(len(origins)):
            first_origin = origins[index]
            max_distance = maximum_distance(first_origin, visits)
            limit_distance = math.ceil(max_distance * multiplier)
            group1, group2 = [], []
            # ordenados de menor distancia a mayor distancia
            for visit in visits:
                distance_to_origin = distance(first_origin, visit)
                if distance_to_origin <= limit_distance:
                    group1.append({
                        'distance': distance_to_origin,
                        'location': visit
                    })
                else:
                    group2.append({
                        'distance': distance_to_origin,
                        'location': visit
                    })

            # tengo ordenadas las visitas por distancia al origen
            group1 = sorted(group1, key=lambda k: k['distance'])
            group2 = sorted(group2, key=lambda k: k['distance'])

            route1 = []
            for item in group1:
                route1.append(item['location'])
            route2 = []
            for item in group2:
                route2.append(item['location'])

            # genera la solucion con los datos obtenidos y la correspondiente inversa
            comments = "Using " + str(multiplier) + " as multiplier. "
            self.create_solution_with_reversed_origins(generation, "DISTANCE_LIMIT", origins, route1, route2, comments)

    # por cada visit tiene que armar un cluster que cumpla que la distancia de los demas visits es
    # menor a la maxima distancia del cluster
    def add_random_cluster_route_to_solution(self, generation, origins, visits):
        if generation.id > 10:
            return
        max_diameter = max_cluster_diameter(visits)
        multiplier = generation.id / (generation.id + 1)
        cluster_diameter = math.ceil(max_diameter * multiplier)

        for location in visits:
            group1 = [location]
            group2 = []

            for neighbor in visits:
                if neighbor.id != location.id:
                    if distance(neighbor, location) <= cluster_diameter:
                        group1.append(neighbor)
                    else:
                        group2.append(neighbor)

            # ordeno por distancia horizontal primero, y por vertical despues
            group1.sort(key=lambda x: (x.horizontal, x.vertical))
            group2.sort(key=lambda x: (x.horizontal, x.vertical))

            # genera la solucion con los datos obtenidos
            comments = "Using " + str(multiplier) + " as multiplier. Starting from location " + str(location.id) + \
                       " at (" + str(location.horizontal) + ", " + str(location.vertical) + "). "

            self.create_solution_with_reversed_origins(generation, "RANDOM_CLUSTER", origins, group1, group2, comments)

    def add_distance_cluster_route_to_solution(self, generation, origins, visits):
        # do the math if the amount of clusters is less than the amount of visits
        if generation.id < len(visits):
            number_of_clusters = generation.id + 1
            for index in range(len(origins)):
                clusters = create_minimal_clusters(list(visits), number_of_clusters)
                route1, route2 = [], []
                counter = len(clusters)
                for cluster in clusters:
                    if counter == 1 and len(route1) == 0:
                        route1.extend(cluster)
                    elif counter == 1 and len(route2) == 0:
                        route2.extend(cluster)
                    elif maximum_distance(origins[index], cluster) < maximum_distance(origins[1 - index], cluster):
                        route1.extend(cluster)
                    else:
                        route2.extend(cluster)
                    counter -= 1
                comments = "Using " + str(number_of_clusters) + " clusters. Calculating from " \
                           + str(origins[index].id) + ". Locations in clusters: "
                for cluster in clusters:
                    comments += "[ "
                    last = len(cluster)
                    for location in cluster:
                        comments += str(location.id)
                        if last > 0:
                            comments += ", "
                            last -= 1
                    comments += "] "
                self.create_solution_with_reversed_origins(generation=generation, source_type="DISTANCE_CLUSTER",
                                                           origins=origins, group1=route1, group2=route2,
                                                           comments=comments)
                # if len(route1) > 0 and len(route2) > 0:
                #    pass
                # else:
                #    logger = tracerlog()
                #    logger.write_info(self.__class__.__name__, "at AddDistanceClusterToSolution(). Not saving routes.")

    def create_solution(self, generation, source_type, origins, group1, group2, comments=""):
        # for index in range(len(origins)):
        new_solution = solution.models.Solution(source=source_type,
                                                generation=generation)
        generation.increment_source_type(source_type)
        new_solution.save()

        first_origin = origins[0]
        second_origin = origins[1]
        new_solution.origins.add(second_origin,
                                 through_defaults={'routeID': 0, 'location': second_origin,
                                                   'solution': new_solution})
        new_solution.origins.add(first_origin,
                                 through_defaults={'routeID': 1, 'location': first_origin,
                                                   'solution': new_solution})
        visit_index = 0
        for item in group1:
            route_id = 0
            new_solution.visits.add(item,
                                    through_defaults={'routeID': route_id, 'index': visit_index, 'location': item,
                                                      'solution': new_solution})
            visit_index += 1
        visit_index = 0
        for item in group2:
            route_id = 1
            new_solution.visits.add(item, through_defaults={'routeID': route_id, 'index': visit_index,
                                                            'location': item, 'solution': new_solution})
            visit_index += 1
        new_solution.cost = self.calculate_cost(new_solution)
        # if index == 1:
        #    comments += "Reversed origins. "
        new_solution.comments = comments
        new_solution.save()
        logger = tracerlog.Log()
        logger.write_info(self.__class__.__name__,
                          "At CreateSolution(). New solution " + str(new_solution.id) + " of type " + source_type + " with cost "
                          + str(new_solution.cost))
        self.log_solution_info(new_solution)

    # [2-4-5-3-2] [1-6-7-8-1]
    # [1-4-5-3-1] [2-6-7-8-2]
    def create_solution_with_reversed_origins(self, generation, source_type, origins, group1, group2, comments=""):
        # solution A
        new_solution = solution.models.Solution(source=source_type, generation=generation)
        generation.increment_source_type(source_type)
        new_solution.save()
        # solution B
        reversed_solution = solution.models.Solution(source=source_type, generation=generation)
        generation.increment_source_type(source_type)
        reversed_solution.save()

        # origins solution A: origin[0] in route 0, origin[1] in route 1
        new_solution.origins.add(origins[0],
                                 through_defaults={'routeID': 0, 'location': origins[0],
                                                   'solution': new_solution})
        new_solution.origins.add(origins[1],
                                 through_defaults={'routeID': 1, 'location': origins[1],
                                                   'solution': new_solution})
        # origins solution B: origin[0] in route 1, origin[1] in route 0
        reversed_solution.origins.add(origins[0],
                                      through_defaults={'routeID': 1, 'location': origins[0],
                                                        'solution': reversed_solution})
        reversed_solution.origins.add(origins[1],
                                      through_defaults={'routeID': 0, 'location': origins[1],
                                                        'solution': reversed_solution})

        # assign visits to routes in the order of the arrays
        groups = [group1, group2]
        for group in groups:
            visit_index = 0
            for item in group:
                route_id = groups.index(group)  # index of group in groups
                new_solution.visits.add(item,
                                        through_defaults={'routeID': route_id, 'index': visit_index, 'location': item,
                                                          'solution': new_solution})
                reversed_solution.visits.add(item, through_defaults={'routeID': route_id, 'index': visit_index,
                                                                     'location': item, 'solution': reversed_solution})
                visit_index += 1
        # calculates cost and adds comments for solution A
        new_solution.cost = self.calculate_cost(new_solution)
        new_solution.comments = comments
        new_solution.save()
        # calculates cost and adds comments for solution B
        reversed_solution.cost = self.calculate_cost(reversed_solution)
        reversed_solution.comments = comments + "Reversed solution. "
        reversed_solution.save()

        # loggers.
        logger = tracerlog.Log()
        logger.write_info(self.__class__.__name__,
                          "At CreateSolutionWithReverse(). New solution " + str(new_solution.id) + " of type " + source_type + " with cost "
                          + str(new_solution.cost))
        logger.write_info(self.__class__.__name__,
                          "At CreateSolutionWithReverse(). New solution " + str(reversed_solution.id) + " of type " + source_type + " with cost "
                          + str(reversed_solution.cost))
        self.log_solution_info(new_solution)
        self.log_solution_info(reversed_solution)

    def log_solution_info(self, new_solution):
        origins = solution.models.OriginLocations.objects.filter(solution=new_solution)

        for route_id in range(len(origins)):
            origin = solution.models.OriginLocations.objects.filter(solution=new_solution, routeID=route_id)
            visit_index = 0
            logger = tracerlog.Log()
            logger.write_generation_info(solution=new_solution, route_id=route_id,
                                         visit_index=visit_index, visit=origin[0].location)
            visits = solution.models.VisitLocations.objects.filter(solution=new_solution, routeID=route_id).order_by(
                "index")
            visit_index += 1
            for item in visits:
                logger.write_generation_info(solution=new_solution, route_id=route_id,
                                             visit_index=visit_index, visit=item.location)
                visit_index += 1
            logger.write_generation_info(solution=new_solution, route_id=route_id,
                                         visit_index=visit_index, visit=origin[0].location)

    def mutate_pool(self, generation, percentage):
        previous_generation_id = generation.id - 1
        solutions = solution.models.Solution.objects.filter(generation__id=previous_generation_id)
        list_of_solutions = list(solutions)
        #print("GENERATION ID "+ str(generation.id))
        #print("LIST OF SOLUTIONS "+ str(len(list_of_solutions)))
        number_of_solutions = len(list_of_solutions) * percentage // 100
        #print("NUMBER OF SOLUTIONS IS " + str(number_of_solutions))
        mutable_solutions = random.sample(list_of_solutions, number_of_solutions)
        for item in mutable_solutions:
            random_choice = random.randint(1, 3)
            origin_group = list(item.origins.all())
            group1 = list(item.visits.filter(visitlocations__routeID=0).order_by('visitlocations__index'))
            group2 = list(item.visits.filter(visitlocations__routeID=1).order_by('visitlocations__index'))
            #print("CHOICE is " + str(random_choice))
            # switch order between 2 visits in each route
            if random_choice == 1:
                indexes1 = []
                if len(group1) >= 2:
                    indexes1 = swap_two_locations_in_route(group1)
                indexes2 = []
                if len(group2) >= 2:
                    indexes2 = swap_two_locations_in_route(group2)
                comments = "Swapped order of " + str(indexes1) + " for route 0 and " \
                           + str(indexes2) \
                           + " for route 1 of solution " + str(item.id) + " with cost " + str(item.cost) + ". "
                self.create_solution_with_reversed_origins(generation, "RANDOM_MUTATION", origin_group, group1, group2,
                                                           comments)
            # move one visit to the other route
            if random_choice == 2:
                picked_route = random.randint(0, 1)
                if (picked_route == 0 and len(group1) >= 2) or (picked_route == 1 and len(group2) < 2):
                    choice = "route1"
                else:
                    choice = "route2"

                if choice == "route1":
                    random_item_from_list = random.choice(group1)
                    group1.remove(random_item_from_list)
                    group2.append(random_item_from_list)
                    comments = "Moved location of id " + str(random_item_from_list.id) \
                               + " from route 0 and appended it to route 1 from solution " + str(item.id) \
                               + " with cost " + str(item.cost) + ". "
                else:
                    random_item_from_list = random.choice(group2)
                    group2.remove(random_item_from_list)
                    group1.append(random_item_from_list)
                    comments = "Moved location of id " + str(random_item_from_list.id) \
                               + " from route 1 and appended it to route 0 from solution " + str(item.id) \
                               + " with cost " + str(item.cost) + ". "
                self.create_solution_with_reversed_origins(generation, "RANDOM_MUTATION", origin_group, group1, group2,
                                                           comments)
            # randomize visits in one route
            if random_choice == 3:
                picked_route = random.randint(0, 1)
                if (picked_route == 0 and len(group1) >= 2) or (picked_route == 1 and len(group2) < 2):
                    choice = "route1"
                else:
                    choice = "route2"
                if choice == "route1":
                    random.shuffle(group1)
                    comments = "Shuffled route 0 from solution " + str(item.id) + " with cost " + str(item.cost) + ". "
                else:
                    random.shuffle(group2)
                    comments = "Shuffled route 1 from solution " + str(item.id) + " with cost " + str(item.cost) + ". "
                self.create_solution_with_reversed_origins(generation, "RANDOM_MUTATION", origin_group, group1, group2,
                                                           comments)
            #print("GENERATION count ", str(generation.number_random_mutation))

    def select_jumpers(self, generation, percentage_good, percentage_med, percentage_bad):
        previous_generation_id = generation.id - 1
        solutions = solution.models.Solution.objects \
            .filter(generation__id=previous_generation_id) \
            .order_by('cost')
        list_of_solutions = list(solutions)
        amount_good_solutions = len(list_of_solutions) * percentage_good // 100
        amount_med_solutions = len(list_of_solutions) * percentage_med // 100
        amount_bad_solutions = len(list_of_solutions) * percentage_bad // 100

        i_good = [0, amount_good_solutions]
        i_med_0 = (len(list_of_solutions) // 2) - amount_med_solutions // 2 - 1
        i_med = [i_med_0, i_med_0 + amount_med_solutions]
        i_bad = [len(list_of_solutions) - amount_bad_solutions - 1, len(list_of_solutions) - 1]

        selected_good = list_of_solutions[i_good[0]:i_good[1]]
        selected_med = list_of_solutions[i_med[0]:i_med[1]]
        selected_bad = list_of_solutions[i_bad[0]:i_bad[1]]

        # print("\GENERATION " + str(generation.id))
        # print("SELECTION. GOOD ITEMS " + str(amount_good_solutions) + " - MED ITEMS " + str(
        #    amount_med_solutions) + " - BAD ITEMS " + str(amount_bad_solutions))
        # print("ACTUAL SELECTION. GOOD ITEMS " + str(len(selected_good)) + " - MED ITEMS " + str(
        #    len(selected_med)) + " - BAD ITEMS " + str(len(selected_bad)))

        for item in selected_good:
            origin_group = list(item.origins.all())
            group1 = list(item.visits.filter(visitlocations__routeID=0).order_by('visitlocations__index'))
            group2 = list(item.visits.filter(visitlocations__routeID=1).order_by('visitlocations__index'))
            comments = "Selected from the best " + str(amount_good_solutions) \
                       + " solutions of the previous generation. " \
                       + "Previous ID was " + str(item.id) + " with cost " + str(item.cost) + ". "
            self.create_solution_with_reversed_origins(generation, "SELECTION", origin_group, group1, group2, comments)

        for item in selected_med:
            origin_group = list(item.origins.all())
            group1 = list(item.visits.filter(visitlocations__routeID=0).order_by('visitlocations__index'))
            group2 = list(item.visits.filter(visitlocations__routeID=1).order_by('visitlocations__index'))
            comments = "Selected from the " + str(amount_med_solutions) \
                       + " average solutions of the previous generation. " \
                       + "Previous ID was " + str(item.id) + " with cost " + str(item.cost) + ". "
            self.create_solution_with_reversed_origins(generation, "SELECTION", origin_group, group1, group2, comments)

        for item in selected_bad:
            origin_group = list(item.origins.all())
            group1 = list(item.visits.filter(visitlocations__routeID=0).order_by('visitlocations__index'))
            group2 = list(item.visits.filter(visitlocations__routeID=1).order_by('visitlocations__index'))
            comments = "Selected from the worst " + str(amount_bad_solutions) \
                       + " solutions of the previous generation. " \
                       + "Previous ID was " + str(item.id) + " with cost " + str(item.cost) + ". "
            self.create_solution_with_reversed_origins(generation, "SELECTION", origin_group, group1, group2, comments)

    def birth_solutions(self, generation, percentage_births):
        previous_generation_id = generation.id - 1
        solutions = solution.models.Solution.objects \
            .filter(generation__id=previous_generation_id) \
            .order_by('cost')
        list_of_solutions = list(solutions)
        amount_of_births = len(list_of_solutions) * percentage_births // 100
        parents_list = []
        # pick parents
        for index in range(amount_of_births):
            random_choice = random.randint(1, 2)
            if random_choice == 1:
                best_solutions = list_of_solutions[0:len(list_of_solutions)//4]
                if len(best_solutions) >=2:
                    parents = random.sample(best_solutions, 2)
                    parents_list.append(parents)
                    logger = tracerlog.Log()
                    logger.write_info(self.__class__.__name__,
                                      "At BirthSolutions(), picking parents. Arranged a marriage between solution "
                                      + str(parents[0].id)
                                      + " with cost "
                                      + str(parents[0].cost)
                                      + " and solution "
                                      + str(parents[1].id)
                                      + " with cost "
                                      + str(parents[1].cost)
                                      + ". "
                                      )
            else:
                if len(list_of_solutions) >= 2:
                    parents = random.sample(list_of_solutions, 2)
                    parents_list.append(parents)
                    logger = tracerlog.Log()
                    logger.write_info(self.__class__.__name__,
                                      "At BirthSolutions(), picking parents. Solutions "
                                      + str(parents[0].id)
                                      + " with cost "
                                      + str(parents[0].cost)
                                      + " and solution "
                                      + str(parents[1].id)
                                      + " with cost "
                                      + str(parents[1].cost)
                                      + " liked each other very much and went ahead and had a baby. "
                                      )
        for parents in parents_list:
            self.create_child_solution(generation, parents)

    def create_child_solution(self, generation, parents):
        # elements in both parents are inherited, the rest is mixed.
        # ABC DEF & AED BCF
        # A__ F__ + BCED? => random assignment
        origin_group = list(parents[0].origins.all())
        origin_routes_match = parents[0].origins.filter(originlocations__routeID=0)[0].id == \
                              parents[1].origins.filter(originlocations__routeID=0)[0].id

        if origin_routes_match:
            sorted_route0 = sort_locations_in_routes_by_shared_or_not(
                parents[0].visits.filter(visitlocations__routeID=0).order_by('visitlocations__index'),
                parents[1].visits.filter(visitlocations__routeID=0).order_by('visitlocations__index')
            )
            sorted_route1 = sort_locations_in_routes_by_shared_or_not(
                parents[0].visits.filter(visitlocations__routeID=1).order_by('visitlocations__index'),
                parents[1].visits.filter(visitlocations__routeID=1).order_by('visitlocations__index')
            )
            leftovers = set(sorted_route0[1] + sorted_route1[1])
        else:
            sorted_route0 = sort_locations_in_routes_by_shared_or_not(
                parents[0].visits.filter(visitlocations__routeID=0).order_by('visitlocations__index'),
                parents[1].visits.filter(visitlocations__routeID=1).order_by('visitlocations__index')
            )
            sorted_route1 = sort_locations_in_routes_by_shared_or_not(
                parents[0].visits.filter(visitlocations__routeID=1).order_by('visitlocations__index'),
                parents[1].visits.filter(visitlocations__routeID=0).order_by('visitlocations__index')
            )
            leftovers = set(sorted_route0[1] + sorted_route1[1])

        route0 = sorted_route0[0]
        route1 = sorted_route1[0]

        comments = "Inherited " + str(route0) + " and " + str(route1) + " from parent solutions " \
                   + str(parents[0].id) \
                   + " and " + str(parents[1].id) + ". "
        for location in leftovers:
            choice = random.randint(0, 1)
            if choice == 0:
                route0.append(location)
            else:
                route1.append(location)

        self.create_solution_with_reversed_origins(generation, "CHILD", origin_group, route0,
                                                   route1, comments)

    def get_best_cost_by_generation(self, generation_id):
        best_solutions = solution.models.Solution.objects.filter(generation__id=generation_id).order_by("cost")[:1]
        print ("BEST SOLUTIONS", best_solutions)
        if len(best_solutions) >= 1:
            print("BEST SOLUTION COST", best_solutions[0].cost)
            return best_solutions[0].cost
        else:
            return -1
