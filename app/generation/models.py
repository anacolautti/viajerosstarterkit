from django.db import models
from .solutionGenerator import SolutionGenerator
from .helpers import BuildHelpers
import run_iteration.models
import tracerlog


class Generation(models.Model):
    total = models.IntegerField(default=0)

    number_random = models.IntegerField(default=0)
    number_distance_limit = models.IntegerField(default=0)
    number_random_cluster = models.IntegerField(default=0)
    number_distance_cluster = models.IntegerField(default=0)
    number_child = models.IntegerField(default=0)
    number_random_mutation = models.IntegerField(default=0)
    number_selection = models.IntegerField(default=0)

    iteration = models.IntegerField(default=0)
    run_iteration = models.ForeignKey(run_iteration.models.RunIteration,
                                      related_name="run_iteration",
                                      on_delete=models.CASCADE,
                                      null=True)

    def set_iteration(self):
        if self.iteration != 0:
            return
        current_run_iteration = run_iteration.models.RunIteration.objects.filter(current=True)
        # Starting a new generation
        if len(list(current_run_iteration)) == 0:
            self.run_iteration = run_iteration.models.RunIteration()
            self.run_iteration.save()
            self.iteration = 1
            self.save()
        # in a currently active run_iteration
        else:
            self.run_iteration = current_run_iteration[0]
            self.run_iteration.save()
            if self.id > 1:
                previous_generation = Generation.objects.filter(id=(self.id - 1))[0]
                self.iteration = previous_generation.iteration + 1
                self.save()
            else:
                self.iteration = 1
                self.save()

    def close_run_iteration(self):
        print("CLOSE GEN ", self.id, "On iteration ", self.iteration)
        self.run_iteration.current = False
        self.run_iteration.save()

    # Should we stop now?
    def should_continue(self, visits):
        logger = tracerlog.Log()
        sg = SolutionGenerator()
        bh = BuildHelpers(self, sg, logger)

        # no use in checking the first one
        if self.iteration > 1:
            if bh.too_many_loops(len(visits) * 2):
                logger.write_info(self.__class__.__name__, "at Build(). Reached loop limit.")
                self.close_run_iteration()
                return False
            # same result in #(visits) loops?
            if bh.same_result_in_number_rows(len(visits)):
                self.close_run_iteration()
                return False
            # ran out of solutions?
            previous_gen = Generation.objects.filter(iteration=self.iteration - 1)[0]
            if bh.no_more_solutions(previous_gen):
                logger.write_info(self.__class__.__name__, "at Build(). Went too far. No more solutions found. ")
                self.close_run_iteration()
                return False
        return True

    def build(self, origins, visits):
        self.set_iteration()
        random_immigrants_multiplier = 5
        number_of_generations_that_allow_immigrants = 3
        immigrants_limit = number_of_generations_that_allow_immigrants + 1  # los IDs empiezan a contar desde 1
        self.build_by_minimum_distance(origins, visits)

        if self.iteration < immigrants_limit:
            self.build_by_random_cluster(origins, visits)
            self.build_by_distance_cluster(origins, visits)
            random_amount = (immigrants_limit - self.iteration) * random_immigrants_multiplier
            for i in range(random_amount):
                self.build_random(origins, visits)
        if self.iteration > 1:
            self.mutate_pool(5)
            self.select_jumpers(6, 2, 2)
            self.birth(40)
        if self.should_continue(visits):
            return True
        else:
            return False

    # adds new_solution to generation
    # visits are arranged in a random order.
    def build_random(self, origins, visits):
        solution_generator = SolutionGenerator()
        solution_generator.add_random_route_to_solution(self, origins=origins, visits=visits)

    # adds new_solution to generation
    # visits are arranged in minimum_distance order.
    # reversed solutions are also built
    def build_by_minimum_distance(self, origins, visits):
        solution_generator = SolutionGenerator()
        solution_generator.add_minimum_distance_route_to_solution(generation=self, origins=origins, visits=visits)

    # adds new_solution to generation
    # visits are arranged in clusters
    # a cluster is generated for each visit, with all the other visits in a distance range
    # one route is built with a cluster in a random order, and the other route is built with
    # the rest of the visits that don't belong to the cluster
    # within the clusters the visits are random
    # solutions are built cross assigning origins for each possible origin order
    def build_by_random_cluster(self, origins, visits):
        solution_generator = SolutionGenerator()
        solution_generator.add_random_cluster_route_to_solution(generation=self, origins=origins, visits=visits)

    # adds new_solution to generation
    # visits are arranged in (gen.id + 1) clusters
    # clusters are minimum (local search)
    # and assigned to routes based on the distance to the origins
    # reversed solutions are also built
    def build_by_distance_cluster(self, origins, visits):
        solution_generator = SolutionGenerator()
        solution_generator.add_distance_cluster_route_to_solution(generation=self, origins=origins, visits=visits)

    def increment_source_type(self, source):
        if source == "RANDOM":
            self.number_random += 1
            self.total += 1
        elif source == "DISTANCE_LIMIT":
            self.number_distance_limit += 1
            self.total += 1
        elif source == "RANDOM_CLUSTER":
            self.number_random_cluster += 1
            self.total += 1
        elif source == "DISTANCE_CLUSTER":
            self.number_distance_cluster += 1
            self.total += 1
        elif source == "CHILD":
            self.number_child += 1
            self.total += 1
        elif source == "RANDOM_MUTATION":
            self.number_random_mutation += 1
            self.total += 1
        elif source == "SELECTION":
            self.number_selection += 1
            self.total += 1

    # adds a solution to the generation based on the previous generation
    # picks a random solution and applies one of the following
    #   - swaps element order in the same route, for both routes
    #   - takes an element from a route and puts it on the other one
    #   - randomizes the order of one route
    def mutate_pool(self, percentage):
        if self.iteration == 1:
            return
        solution_generator = SolutionGenerator()
        solution_generator.mutate_pool(self, percentage)

    # picks solutions to survive a generation
    # based on the percentages provided
    def select_jumpers(self, percentage_good, percentage_med, percentage_bad):
        if self.iteration == 1:
            return
        solution_generator = SolutionGenerator()
        solution_generator.select_jumpers(self, percentage_good, percentage_med, percentage_bad)

    # creates solutions based on the solutions from the previous generation
    # each new route inherits the locations from the parents routes if they are shared
    # the rest of the locations are assigned randomly
    def birth(self, percentage_births):
        if self.iteration == 1:
            return
        solution_generator = SolutionGenerator()
        solution_generator.birth_solutions(self, percentage_births)
