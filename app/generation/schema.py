import graphene
from graphene_django import DjangoObjectType
from .models import Generation
import run_iteration.models
import location.models


class GenerationType(DjangoObjectType):
    class Meta:
        model = Generation


class Query(graphene.ObjectType):
    generations = graphene.List(GenerationType, run_id=graphene.Int())

    def resolve_generations(self, info, run_id=None):
        if run_id is None:
            current_run_iteration = run_iteration.models.RunIteration.objects.order_by('-id')[0]
            return Generation.objects.filter(run_iteration__id=current_run_iteration.id)
        else:
            return Generation.objects.filter(run_iteration__id=run_id)


class CreateGenerations(graphene.Mutation):
    generations = graphene.List(GenerationType)

    class Arguments:
        iterations = graphene.Int()

    def mutate(self, info, iterations=1):
        print("Requested number of iterations is ", iterations)
        generations = []
        origins = location.models.Location.objects.filter(type="init")
        visits = location.models.Location.objects.filter(type="visit")
        for _ in range(iterations):
            generation = Generation()
            generation.save()
            should_keep_going = generation.build(origins, visits)
            generation.save()
            generations.append(generation)
            if not should_keep_going:
                break

        return CreateGenerations(generations)


class Mutation(graphene.ObjectType):
    create_generations = CreateGenerations.Field()
