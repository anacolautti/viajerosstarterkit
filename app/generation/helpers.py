import math
import random

# Calcula distancias entre locations
# coordinates YA NO SON son del tipo {"x": 1, "y": 1}
def distance(locationStart, locationEnd):
    horizontal = abs(locationEnd.horizontal - locationStart.horizontal)
    vertical = abs(locationEnd.vertical - locationStart.vertical)
    return horizontal + vertical


# Calcula la distancia máxima entre locations
# coordinates YA NO SON son del tipo {"x": 1, "y": 1}
def maximum_distance(origin, locations):
    max_distance = 0
    for step in locations:
        if abs(step.horizontal - origin.horizontal) + abs(step.vertical - origin.vertical) \
                > max_distance:
            max_distance = abs(step.horizontal - origin.horizontal) + abs(step.vertical - origin.vertical)
    return max_distance


# Calcula el diametro del clúster más grande de locations
# coordinates YA NO SON son del tipo {"x": 1, "y": 1}
def max_cluster_diameter(locations):
    minX = min(item.horizontal for item in locations)
    maxX = max(item.horizontal for item in locations)
    minY = min(item.vertical for item in locations)
    maxY = max(item.vertical for item in locations)
    return abs(maxX - minX) + abs(maxY - minY)


def create_minimal_clusters(visits, number_of_clusters):
    items_per_cluster = len(visits) // number_of_clusters
    if items_per_cluster == 0:
        items_per_cluster = 1

    raw_clusters = []
    visited = 0
    sample_locations = visits.copy()
    while visited < len(visits):
        print("VISITED ", visited)
        print("VISITS", len(visits))
        print("HERE", len(sample_locations), " - ", items_per_cluster)
        if len(sample_locations) == 0:
            break
        if items_per_cluster <= len(sample_locations):
            cluster = random.sample(sample_locations, items_per_cluster)
        else:
            cluster = sample_locations.copy()
        raw_clusters.append(cluster)
        for location in cluster:
            sample_locations.remove(location)
        visited += len(cluster)
    print("visited at the end ", visited )
    return reduce_clusters_local_search(raw_clusters)


def reduce_clusters_local_search(clusters):
    cluster_len = len(clusters)
    cluster_solutions = []
    local_search_clusters = clusters.copy()
    for k in range(10):
        for j in range(10):
            cluster_solution = []
            for i in range(cluster_len):
                element = {'max': max_cluster_diameter(local_search_clusters[i]), 'cluster': local_search_clusters[i]}
                cluster_solution.append(element)
            cluster_solutions.append(cluster_solution)
            local_search_clusters = local_switch(local_search_clusters)
        local_search_clusters = minimum_clusters(cluster_solutions)
    return local_search_clusters


def local_switch(clusters):
    # shift one element to the next cluster
    elements = []
    local_clusters = clusters.copy()
    for cluster in local_clusters:
        element = random.choice(cluster)
        elements.append(element)
        cluster.remove(element)
    switched_clusters = []
    for cluster in local_clusters:
        current_choice = random.choice(elements)
        cluster.append(current_choice)
        elements.remove(current_choice)
    return local_clusters


def minimum_clusters(cluster_solutions):
    best_solution = []
    best_value = 0
    first_solution = cluster_solutions[0]
    for element in first_solution:
        best_value += element['max']
        best_solution.append(element['cluster'])
    for solution in cluster_solutions:
        current_value = 0
        current_solution = []
        for element in solution:
            current_value += element['max']
            current_solution.append(element['cluster'])
        if current_value < best_value and current_value != 0:
            best_value = current_value
            best_solution = current_solution.copy()
    return best_solution


def swap_two_locations_in_route(route):
    random_index = random.randint(0, len(route)-1)
    next_index = random_index + 1
    if next_index >= len(route):
        next_index = 0
    route[random_index], route[next_index] = route[next_index], route[random_index]
    return [random_index+1, next_index+1]


def sort_locations_in_routes_by_shared_or_not(route1, route2):
    shared = []
    not_shared = []
    locations = []
    r1 = list(route1).copy()
    r2 = list(route2).copy()
    while len(r1) > 0 or len(r2) > 0:
        if len(r1) > 0:
            locations.append(r1.pop(0))
        if len(r2) > 0:
            locations.append(r2.pop(0))
    for visit in locations:
        if (visit in route1) and (visit in route2):
            if visit not in shared:
                shared.append(visit)
        else:
            if visit not in not_shared:
                not_shared.append(visit)
    return [shared, not_shared]


class BuildHelpers:
    def __init__(self, generation, solution_generator, logger):
        self.gen_id = generation.id
        self.iteration = generation.iteration
        self.sg = solution_generator
        self.log = logger

    def are_elements_sorted(self, array):
        previous = array[0]
        for item in array:
            print ("COMPARISON ", previous, item)
            if previous < item:
                return False
            previous = item
        return True

    def are_elements_equal(self, array):
        previous = array[0]
        for item in array:
            print ("EQ COMPARISON ", previous, item)
            if previous != item:
                return False
            previous = item
        return True

    def too_many_loops(self, limit):
        if self.iteration >= limit:
            return True
        else:
            return False

    def gen_id_minus_element(self, number):
        return self.gen_id - number

    def same_result_in_number_rows(self, rows):
        # has enough generations to check
        if self.iteration > rows:
            indexes = list(range(0, rows))
            indexes.reverse()
            ids = list(map(self.gen_id_minus_element, indexes))
            print(ids)

            best_solution_cost = []
            for gen_id in ids:
                best_solution_cost.append(self.sg.get_best_cost_by_generation(generation_id=gen_id))
            print(best_solution_cost)

            if self.are_elements_sorted(best_solution_cost) and self.are_elements_equal(best_solution_cost):
                print("ENTRO PAPA")
                log_message = "at Build(). At iteration " + str(self.iteration) + ". Not improving cost for " + str(len(ids)) + " generations. "
                for index in range(0, len(ids)):
                    log_message += "Generation " + str(ids[index]) + " has best cost at " + str(
                        best_solution_cost[index]) + ". "
                self.log.write_info(self.__class__.__name__, log_message)
                print("REACHED SAME RESULT")
                return True

    def no_more_solutions(self, previous_generation):
        if previous_generation.iteration > 1 and previous_generation.total < 4:
            return True
        else:
            return False
