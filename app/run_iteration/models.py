from django.db import models


class RunIteration(models.Model):
    current = models.BooleanField(default=True)
