from django.apps import AppConfig


class RunIterationConfig(AppConfig):
    name = 'run_iteration'
