import graphene
from graphene_django import DjangoObjectType
from .models import RunIteration


class RunIterationType(DjangoObjectType):
    class Meta:
        model = RunIteration


class Query(graphene.ObjectType):
    run_iteration = graphene.List(RunIterationType)

    def resolve_run_iteration(self, info):
        return RunIteration.objects.all()
