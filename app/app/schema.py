import graphene
import location.schema
import solution.schema
import generation.schema
import run_iteration.schema


class Query(location.schema.Query,
            solution.schema.Query,
            generation.schema.Query,
            run_iteration.schema.Query,
            graphene.ObjectType):
    pass


class Mutation(location.schema.Mutation,
               solution.schema.Mutation,
               generation.schema.Mutation, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
