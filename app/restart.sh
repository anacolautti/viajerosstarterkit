#!/bin/bash

rm db.sqlite3
rm generation/migrations/000*
rm solution/migrations/000*
rm location/migrations/000*
rm run_iterations/migrations/000*
d=`date +%s`
mv logfile.csv logfile-$d.csv
mv generation_logfile.csv generation_logfile-$d.csv

python manage.py makemigrations
python manage.py migrate
python manage.py loaddata locations.json



