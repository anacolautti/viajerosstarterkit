# Run python app:

## Start the server

`python manage.py runserver`

## To restart the database and run the program:

`bash restart.sh`

# GraphQL queries

## Create location

```
# Creates a location of type "visit"
#
# Requires filter
#   - horizontal(float)
#   - vertical(float)
#
mutation {
  createLocation(horizontal:3, vertical:7) {
    location {
      id
    }
  }
}
```

## Delete location

```
# Deletes an existing location
#
# Requires filter
#   - locationId(int)
#
mutation {
  deleteLocation(locationId:3) {
    locationId
  }
}
```

## List all locations

```
# Lists all existing locations
#
# No parameters.

{
  locations {
    id
    horizontal
    vertical
    type
  }
}
```

## Create generations

```
# The parameter iterations is optional. If it's not provided 
# the query will run one iteration.

mutation {
  createGenerations(iterations:2) {
    generations {
      iteration
      runIteration {
        id
      }
      total
    }
  }
}
```

## List existing generations

```
# Lists all generations in the current run.
#
# Available filters are
#   - runId (int). Defaults to latest run.
#

{
  generations {
    iteration
    runIteration{
      id
      current
    }

    total

		numberSelection
    numberRandom
    numberChild
    numberDistanceLimit
    numberRandomCluster
    numberRandomMutation
    numberDistanceCluster
  }
}
```

## List all solutions

```
# Lists all solutions in the current/latest run 
#
# Available filters are
#   - source (string). Available values are:
#			  RANDOM
#			  DISTANCE_LIMIT
#			  DISTANCE_CLUSTER
#			  RANDOM_CLUSTER
#		  	RANDOM_MUTATION
#			  CHILD
#			  SELECTION
#		- generationIteration (int)
#
# These filters can be used together (AND operation)

{
  solutions(source:"DISTANCE_LIMIT", generationIteration:1) {
    id
    origins {
      horizontal
      vertical
    }
    visits {
      horizontal
      vertical
    }
    cost
    source
    generation {
      iteration
      runIteration {
        id
      }
    }
  }
}
```

## Best solution

```
# Returns all the solutions with the best cost in the latest iteration
# from the current run
#
# Available filters are
#   - generationIteration: int
#			  returns best solution in that generation
#   - allGenerations: boolean
# 		  returns best solutions across all the generations 
#       in the latest run
#   - runId: int
#			  returns best solutions in a given run
#   - none
#
# Filters cannot be used together.


{
  bestSolution(generationIteration:2) {
    id
    cost
    generation {
      iteration
      runIteration {
        id
        current
      }
    }
  }
}

```

## Relationships between solutions and locations

```
# Lists relationships between originLocations/visitLocations 
# and Locations and Solutions
{
  originLocations {
    routeID
    location{
      id
      horizontal
      vertical
    }
    solution {
      id
    }
  }
  visitLocations {
    routeID
    index
    location {
      id
      horizontal
      vertical
    }
  }
}
```
