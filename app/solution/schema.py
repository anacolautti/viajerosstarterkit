import graphene
from graphene_django import DjangoObjectType
from .models import Solution, OriginLocations, VisitLocations
from location.models import Location
from run_iteration.models import RunIteration
from django.db.models import Q


class SolutionType(DjangoObjectType):
    class Meta:
        model = Solution


class OriginLocationsType(DjangoObjectType):
    class Meta:
        model = OriginLocations


class VisitLocationsType(DjangoObjectType):
    class Meta:
        model = VisitLocations


class Query(graphene.ObjectType):
    solution = graphene.List(SolutionType, id=graphene.Int(required=True))
    solutions = graphene.List(SolutionType, generation_iteration=graphene.Int(), source=graphene.String())
    best_solution = graphene.List(SolutionType, generation_iteration=graphene.Int(), run_id=graphene.Int(),
                                  all_generations=graphene.Boolean())
    origin_locations = graphene.List(OriginLocationsType)
    visit_locations = graphene.List(VisitLocationsType)

    def resolve_solution(self, info, id=None):
        my_filter = (
            Q(id=id, generation__run_iteration__current=True)
        )
        return Solution.objects.filter(my_filter)

    def resolve_solutions(self, info, generation_iteration=None, source=None, **kwargs):
        run_iterations = RunIteration.objects.order_by('-id')
        if len(run_iterations) == 0:
            return []
        last_run = run_iterations[0]
        print(last_run.id)
        if generation_iteration and source:
            my_filter = (
                Q(generation__iteration=generation_iteration) & Q(source=source)
                & Q(generation__run_iteration__id=last_run.id)
            )
            return Solution.objects.filter(my_filter)
        if source:
            my_filter = (
                Q(generation__run_iteration__id=last_run.id) & Q(source=source)
            )
            return Solution.objects.filter(my_filter)
        if generation_iteration:
            my_filter = (
                Q(generation__iteration=generation_iteration) & Q(generation__run_iteration__id=last_run.id)
            )
            return Solution.objects.filter(my_filter)
        return Solution.objects.filter(generation__run_iteration__id=last_run.id)

    # cambiar generation_id por generation_iteration.
    def resolve_best_solution(self, info, generation_iteration=None, all_generations=None, run_id=None):
        run_iterations = RunIteration.objects.order_by('-id')
        if len(run_iterations) == 0:
            return []
        last_run = run_iterations[0]
        print(last_run.id)
        if generation_iteration:
            my_filter = (
                Q(generation__iteration=generation_iteration) & Q(generation__run_iteration__id=last_run.id)
            )
            return Solution.objects.filter(my_filter).order_by("cost")
        elif all_generations:
            # in the latest run
            max_generations_solutions_list = Solution.objects.filter(generation__run_iteration__id=last_run.id)\
                                                     .order_by("-generation__iteration")
            result = []
            iterations = max_generations_solutions_list[0].generation.iteration
            while iterations > 0:
                best_solution = Solution.objects.filter(generation__run_iteration__id=last_run.id,
                                                        generation__iteration=iterations)\
                                                .order_by("cost")[0]
                result.append(best_solution)
                iterations -= 1
            return result
        elif run_id:
            max_generations_solutions_list = Solution.objects.order_by("cost")###
        else:
            max_generations_solutions_list = Solution.objects.filter(generation__run_iteration__id=last_run.id) \
                                                      .order_by("-generation__id", "cost")[:1]
        best_solutions_list = []
        if len(max_generations_solutions_list) > 0:
            max_generation = max_generations_solutions_list[0].generation.id
            min_cost = max_generations_solutions_list[0].cost
            best_solutions_list = Solution.objects.filter(generation__id=max_generation, cost=min_cost)
        return best_solutions_list

    def resolve_origin_locations(self, info):
        return OriginLocations.objects.all()

    def resolve_visit_locations(self, info):
        visit_locations = VisitLocations.objects.all()
        return visit_locations


class CreateSolution(graphene.Mutation):
    solution = graphene.Field(SolutionType)

    class Arguments:
        source = graphene.String()

    def mutate(self, info, source):
        origins = Location.objects.filter(type="init")
        visits = Location.objects.filter(type="visit")

        solution = Solution(source=source)
        solution.save() # asi le pone un id
        solution.build_solution_routes(origins, visits)
        return CreateSolution(solution)


class Mutation(graphene.ObjectType):
    # create_solution = CreateSolution.Field()
    pass

