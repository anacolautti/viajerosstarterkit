from django.db import models
import location.models
import generation.models


class Solution(models.Model):
    class SolutionSource(models.TextChoices):
        RANDOM = "RANDOM"
        DISTANCE_LIMIT = "DISTANCE_LIMIT"
        RANDOM_CLUSTER = "RANDOM_CLUSTER"
        DISTANCE_CLUSTER = "DISTANCE_CLUSTER"
        RANDOM_MUTATION = "RANDOM_MUTATION"
        CHILD = "CHILD"
        SELECTION = "SELECTION"

    cost = models.IntegerField(default=1000000)
    source = models.CharField(
        max_length=32,
        choices=SolutionSource.choices,
        default=SolutionSource.RANDOM
    )
    comments = models.TextField(blank=True)
    origins = models.ManyToManyField(location.models.Location, related_name="origins", through='OriginLocations')
    visits = models.ManyToManyField(location.models.Location, related_name="visits", through='VisitLocations')
    generation = models.ForeignKey(generation.models.Generation,
                                   related_name="generation",
                                   on_delete=models.CASCADE,
                                   null=True)

    def get_origins(self):
        origins = self.origins.order_by('visitlocations__routeID')
        return origins

    def get_visits(self):
        visits = []
        visits.append(self.visits.filter(visitlocations__routeID=0).order_by('visitlocations__index'))
        visits.append(self.visits.filter(visitlocations__routeID=1).order_by('visitlocations__index'))
        return visits

    # returns like [[origin, visit, visit, visit, origin][origin, visit, visit, visit, origin]]
    def get_routes(self):
        origins = self.get_origins()
        visits = self.get_visits()
        routes = []
        route_index = 0
        for origin in origins:
            route = [origin]
            for visit in visits[route_index]:
                route.append(visit)
            route.append(origin)
            routes.append(route)
            route_index += 1
        return routes


class OriginLocations(models.Model):
    routeID = models.IntegerField(default=-1)
    location = models.ForeignKey(location.models.Location, on_delete=models.CASCADE, null=True)
    solution = models.ForeignKey(Solution, on_delete=models.CASCADE, null=True)


class VisitLocations(models.Model):
    routeID = models.IntegerField(default=-1)
    index = models.IntegerField(default=-1)
    location = models.ForeignKey(location.models.Location, on_delete=models.CASCADE, null=True)
    solution = models.ForeignKey(Solution, on_delete=models.CASCADE, null=True)
