from django.db import models


class Location(models.Model):
    horizontal = models.FloatField()
    vertical = models.FloatField()
    type = models.CharField(max_length=64, default="visit")
    # title = models.CharField(max_length=64)
    # description = models.TextField(blank=True)
    # url = models.DateTimeField(auto_now_add=True)
