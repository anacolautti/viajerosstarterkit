import graphene
from graphene_django import DjangoObjectType
from .models import Location
from run_iteration.models import RunIteration


class LocationType(DjangoObjectType):
    class Meta:
        model = Location


class Query(graphene.ObjectType):
    locations = graphene.List(LocationType)

    def resolve_locations(self, info):
        return Location.objects.all()


class CreateLocation(graphene.Mutation):
    location = graphene.Field(LocationType)

    class Arguments:
        horizontal = graphene.Float()
        vertical = graphene.Float()

    def mutate(self, info, **kwargs):
        location = Location(horizontal=kwargs.get('horizontal'), vertical=kwargs.get('vertical'))
        location.save()
        run_iterations = RunIteration.objects.all()
        for run_iteration in run_iterations:
            run_iteration.current = False
            print("Closing current iteration")
            run_iteration.save()
        return CreateLocation(location)


class DeleteLocation(graphene.Mutation):
    location_id = graphene.Int()

    class Arguments:
        location_id = graphene.Int(required=True)

    def mutate(self, info, location_id):
        location = Location.objects.filter(id=location_id)
        if location:
            run_iterations = RunIteration.objects.all()
            for run_iteration in run_iterations:
                run_iteration.current = False
                print("Closing current iteration")
                run_iteration.save()
            location.delete()
        else:
            print("\n\nunable to delete location, invalid ID " + str(location_id) + "\n\n")
        return DeleteLocation(location_id=location_id)


class UpdateLocation(graphene.Mutation):
    location_id = graphene.Int()

    class Arguments:
        location_id = graphene.Int(required=True)
        horizontal = graphene.Float(required=True)
        vertical = graphene.Float(required=True)

    def mutate(self, info, location_id, horizontal, vertical):
        location = Location.objects.filter(id=location_id)
        if location:
            run_iterations = RunIteration.objects.all()
            for run_iteration in run_iterations:
                run_iteration.current = False
                print("Closing current iteration")
                run_iteration.save()
            location[0].horizontal = horizontal
            location[0].vertical = vertical
            location[0].save()
        else:
            print("\n\nunable to edit location, invalid ID " + str(location_id) + "\n\n")
        return UpdateLocation(location_id=location_id)


class Mutation(graphene.ObjectType):
    create_location = CreateLocation.Field()
    delete_location = DeleteLocation.Field()
    update_location = UpdateLocation.Field()
