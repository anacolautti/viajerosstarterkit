import React from "react";
import withRoot from "./withRoot";

import Header from "./components/Shared/Header"

import App from "./pages/App"

const Root = () => {

    return (
    <div id="page">
        <Header></Header>
        <App></App>
    </div>
    )
};

export default withRoot(Root);
