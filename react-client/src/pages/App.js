import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";

import { Query } from "react-apollo"
import { gql } from "apollo-boost"

import Loading from "../components/Shared/Loading"
import Error from "../components/Shared/Error"

import LocationList, { GET_LOCATIONS_QUERY } from "../components/Location/LocationList"
import SolutionList from "../components/Solution/SolutionList"
import Map2d from "../components/Map2d/Map2d"
import CreateGeneration from "../components/Generation/CreateGeneration"
import Grid from '@material-ui/core/Grid';
// TODO AVISAR QUE TERMINO LA GENERACION

function buildSolutionRoutes(solution) {
  let routes = []
  solution.origins.forEach(origin => {
    routes[origin.routeID] = [origin.location]
  });
  solution.visits.forEach(visit => {
    routes[visit.routeID].push(visit.location)
  });

  solution.origins.forEach(origin => {
    let index = origin.routeID
    routes[index].push(origin.location)
  });

  return routes
}

function listBestSolutionsByGenerations(solutions) {
  let result=[]
  
  solutions.forEach(element => {
    let solution_iteration = element.generation.iteration
    let not_added = true
    result.forEach(element => {
      if(element.generation.iteration === solution_iteration) {
        not_added = false
      }
    });
    if (not_added) result.push(element);
    
  });
    
  return result
}

function getOriginsFromLocations(locations) {
  let originList = []
  locations.forEach(element => {
    if (element.type==="init") {
      originList.push(element)
    }
  });
  return originList
}

function getVisitsFromLocations(locations) {
  let visitList = []
  locations.forEach(element => {
    if (element.type==="visit") {
      visitList.push(element)
    }
  });
  return visitList 
}

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedSolutionID: 0,
      selected: false,
      origins: [],
      visits: [],
      routes: [],
      solutionList: [],
      draw: true
    }
  }

  setSolutionList = (newList) => {
    this.setState({...this.state, solutionList: newList })
  }

  setSelected = (value) => {
    this.setState({...this.state, selected: value })
  }

  setSelectedSolutionID = (value) => {
    this.setState({...this.state, selectedSolutionID: value })
  }

  onGenerationUpdate = () => {
    this.setState({...this.state, 
      draw: true
    })
    console.log(this.state)
  }


  onLocationUpdate = () => {
    this.setState({...this.state, 
      selectedSolutionID: 0,
      selected: false,
      routes: [],
      solutionList: [],
      draw: false
    })
    console.log(this.state)
  }

  onSolutionClick = (id) => {
    let solution = this.state.solutionList[0]
    this.state.solutionList.forEach(element => {
      if (element.id === id) {
        solution = element
      }
    });
    this.setState({...this.state, 
      selectedSolutionID: id,
      selected: true,
      routes: buildSolutionRoutes(solution)
    })
  }

  setRoutes = (newRoutes) => {
    this.setState({...this.state, routes: newRoutes })
  }

  setLocations = (newLocations) =>{
    let newOrigins = getOriginsFromLocations(newLocations)
    let newVisits = getVisitsFromLocations(newLocations)
    this.setState({...this.state, origins: newOrigins, visits: newVisits })
  }

  render() {
    const onSolutionClick = this.onSolutionClick
    
    return (
      <div style={{ display:"flex", justifyContent:"space-around"}}>
        <LocationList  onLocationUpdate={ this.onLocationUpdate } />
        <Query query={GET_LOCATIONS_QUERY} onCompleted={(data)=>{
            this.setLocations(data.locations)
          }}>
          {({data, loading, error}) => {
            if (loading) return <Loading></Loading>
            if (error) return <Error error={error}></Error>
            
            return(
              <Query
                query={GET_SOLUTIONS_QUERY} style={{ flexGrow:"1" }} onCompleted={(data)=>{
                  if (data.bestSolutions.length !== 0){
                    data.bestSolutions.sort((solutionA, solutionB) => {// ordeno de mayor iteracion a menor iteracion
                      return (solutionA.generation.iteration > solutionB.generation.iteration) ? 0 : 1;
                    })
                    this.setSolutionList(listBestSolutionsByGenerations(data.bestSolutions))
                    if (!this.state.selected) {
                      this.setSelectedSolutionID(data.bestSolutions[0].id)
                    }
                  }
                }}
              >
              {({data, loading, error}) => {
                  if (loading) return <Loading></Loading>
                  if (error) {
                    return <Error error={error}></Error>
                  }
                  return(
                    <Query 
                      query={GET_SOLUTION_BY_ID} 
                      variables={{ solutionId: this.state.selectedSolutionID }} 
                      style={{ flexGrow:"1" }}
                      onCompleted={(data)=>{
                        if ((data.solution.length !== 0) && !this.state.selected) {
                          this.setRoutes(buildSolutionRoutes(data.solution[0]))
                        }
                      }}
                      >
                      {({data, loading, error}) => {
                        if (loading) return <Loading></Loading>
                        if (error) {
                          return <Error error={error}></Error>
                        }
                        console.log("STATE", this.state)
                        return(
                          <div aria-roledescription="main" style={{display:"flex", flexGrow:"0.7", justifyContent:"space-evenly"}}> 

                            <Grid container spacing={3}>
                              <Grid item xs={12} sm={9}>
                                <div style={{ position: "fixed", 
                                              margin: "2% auto", padding: "10px",
                                              border: "5px solid #ccc", backgroundColor: "#fff" }}>
                                  <Map2d origins={this.state.origins} visits={this.state.visits} routes={this.state.routes} draw={this.state.draw}></Map2d>
                                  <div style={{display:"flex", justifyContent:"center"}}> 
                                    <CreateGeneration  onListClick={this.onGenerationUpdate} />
                                  </div>
                                </div>
                              </Grid>
                              <Grid item xs={12} sm={3}>
                                { (this.state.solutionList.length !== 0 && this.state.draw) ? <SolutionList solutions={this.state.solutionList} onListClick={onSolutionClick} selectedSolutionId={this.state.selectedSolutionID}></SolutionList> : null }
                              </Grid>
                            </Grid>
                          </div>
                        )
                      }}
                    </Query>
                  )
                }}
              </Query>
            )

          }}
        </Query>
        
      </div>
    );
  }
};

const GET_SOLUTION_BY_ID = gql`
query solution($solutionId: Int!) {
  solution(id:$solutionId) {
    id
    origins: originlocationsSet {
      routeID
      location {
        id
        horizontal
        vertical
      }
    }
    visits: visitlocationsSet {
      routeID
      index
      location {
        id
        horizontal
        vertical
      }
    }
    cost
    source
    generation {
      iteration
      runIteration {
        id
      }
    }
  }
  bestSolutions: bestSolution(allGenerations:true) {
    id
    cost
    source
    generation {
      iteration
      runIteration {
        id
        current
      }
    }
    origins: originlocationsSet {
      routeID
      location {
        id
        horizontal
        vertical
      }
    }
    visits: visitlocationsSet {
      routeID
      index
      location {
        id
        horizontal
        vertical
      }
    }
  }
}`


const styles = theme => ({
  container: {
    margin: "0 auto",
    maxWidth: 960,
    padding: theme.spacing.unit * 2
  }
});

export const GET_SOLUTIONS_QUERY = gql`
{
  bestSolutions: bestSolution(allGenerations:true) {
    id
    cost
    source
    generation {
      iteration
      runIteration {
        id
        current
      }
    }
    origins: originlocationsSet {
      routeID
      location {
        id
        horizontal
        vertical
      }
    }
    visits: visitlocationsSet {
      routeID
      index
      location {
        id
        horizontal
        vertical
      }
    }
  }
}
`

export default withStyles(styles)(App);
