import React from 'react'
import { Graphics } from '@inlet/react-pixi';


const Grid = (props) => {
    return (
        <Graphics
          draw={g => {        
          const width = props.width;
          const height = props.height;
          const dh = props.scale;
          const dv = props.scale;
  
          const hLines = Math.floor(height / dh);
          const vLines= Math.floor(width / dv);
  
          g.clear();
  
          g.lineStyle(1, 0x000000, 1);
  
          //Dibujo líneas horizontales
          for (let i = 1; i < hLines; i++) {
            let p = dh * i;
            g.moveTo(0,p);
            g.lineTo(width,p);
          }
  
          //Dibujo líneas verticales
          for (let i = 1; i < vLines; i++) {
            let p = dv * i;
            g.moveTo(p,0);
            g.lineTo(p,height);
          }

          g.drawRect(1, 1, width, height);
          

          //coloredPath(g, dh, dv, props.routes[0], 0x0abb00); // green
          
          //coloredPath(g, dh, dv, props.routes[1], 0x0f21a9); // blue
  /*

          g.moveTo(0,0);
          g.lineTo(200,0);
  
  
          g.beginFill(0xff3300);
          g.lineStyle(4, 0xffd900, 1);
  
          g.moveTo(50, 50);
          g.lineTo(250, 50);
          g.lineTo(100, 100);
          g.lineTo(50, 50);
          g.endFill();
  
          g.lineStyle(2, 0x0000ff, 1);
          g.beginFill(0xff700b, 1);
          g.drawRect(50, 250, 120, 120);
  
          g.lineStyle(2, 0xff00ff, 1);
          g.beginFill(0xff00bb, 0.25);
          g.drawRoundedRect(250, 200, 200, 200, 15);
          g.endFill();
          
          g.lineStyle(0);
          g.beginFill(0xffff0b, 0.5);
          g.drawCircle(200, 90, 60);
          g.endFill();
      */
        }}
      />
    )
}

export default Grid;