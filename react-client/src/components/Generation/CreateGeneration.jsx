import React, { useState } from "react";
import { Mutation } from "react-apollo";
import { gql } from "apollo-boost";
import withStyles from "@material-ui/core/styles/withStyles";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import AddIcon from "@material-ui/icons/Add";
import { GET_LOCATIONS_QUERY } from "../Location/LocationList"


import { GET_SOLUTIONS_QUERY } from "../../pages/App"
import Loading from "../Shared/Loading"
import Error from "../Shared/Error"

const CreateGeneration = ({ classes, onListClick }) => {
  const [open, setOpen] = useState(false)
  const [amount, setAmount] = useState(1)
  const [startOver, setStartOver] = useState(false)
  const startOverMessage = "Reiniciar"
  const defaultMessage = "Nueva generación"

  const handleSubmit = (event, createGenerations) => {
    event.preventDefault(); // prevents the page from reloading
    createGenerations({variables: {amount}})
    onListClick(0) 
  }

  return (
    <React.Fragment>
      <Button onClick={
        ()=> setOpen(true) } variant="contained" color="primary" className={classes.fab}>
          <AddIcon></AddIcon>
          {startOver ? `${startOverMessage}` : `${defaultMessage}`}
      </Button>


      <Mutation 
        mutation={CREATE_GENERATION_MUTATION}
        onCompleted={data => {
          console.log({data});
          let checker = true
          data.createGenerations.generations.forEach(element => {
            if(!startOver && !element.runIteration.current) {
              setStartOver(true)
              checker = false
            }
          });
          if (checker) {
            setStartOver(false)
          }
          setOpen(false);
        }}
        refetchQueries={() => [ { query: GET_SOLUTIONS_QUERY }]}
        >
        {(createGenerations, { loading, error }) => {
          if (error) return <Error error={error}/>
          if (loading) return <Loading></Loading>

          return(
            <Dialog open={open} className={classes.dialog}>
              <form onSubmit={event => handleSubmit(event, createGenerations)}>
              <DialogTitle> {startOver ? `${startOverMessage}` : `${defaultMessage}`}</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Opciones
                    </DialogContentText>
                    <FormControl fullWidth>
                        <input type="text" pattern="[0-9]*.[0-9]*"
                          label="Cantidad de generaciones"
                          placeholder={amount}
                          onChange={ event => setAmount(event.target.value) }
                          className={classes.TextField}
                        />
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button 
                      onClick={()=> setOpen(false) } 
                      className={classes.cancel}
                    >
                      Cancelar
                    </Button>
                    <Button 
                      disabled={ !amount } 
                      type="submit" 
                      onClick={()=> setOpen(false) }
                      className={classes.save}
                    >
                      Generar
                    </Button>
                </DialogActions>
              </form>
            </Dialog>
          );
        }}
      </Mutation>



    </React.Fragment>
  );
};


const CREATE_GENERATION_MUTATION = gql`
  mutation($amount: Int) {
    createGenerations(iterations: $amount) {
      generations {
        iteration
        runIteration {
          id
          current
        }
        total
      }
    }
  }
`

const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  dialog: {
    margin: "0 auto",
    maxWidth: 550
  },
  textField: {
    margin: theme.spacing.unit
  },
  cancel: {
    color: "red"
  },
  save: {
    color: "green"
  },
  button: {
    margin: theme.spacing.unit * 2,
    marginBottom: "16px"
  },
  icon: {
    marginLeft: theme.spacing.unit
  },
  input: {
    display: "none"
  },
  fab: {
    position: "fixed",
    bottom: theme.spacing.unit * 2,
    //left: theme.spacing.unit * 2,
    zIndex: "200"
  }
});


export default withStyles(styles)(CreateGeneration);


/*
{
  bestSolution(allGenerations: true) {
    id
    cost
    originlocationsSet {
      location {
        id
      }
      routeID
    }
		visitlocationsSet {
      location {
        id
      }
			routeID
      index
    }
    generation {
      iteration
      runIteration {
        id
        current
      }
    }
  }
}

*/