import React, { useState } from "react";
import IconButton from "@material-ui/core/IconButton";
//import TrashIcon from "@material-ui/icons/DeleteForeverOutlined";
import EditIcon from '@material-ui/icons/Edit';
import { GET_LOCATIONS_QUERY } from "./LocationList"
import withStyles from "@material-ui/core/styles/withStyles";

import { Mutation } from "react-apollo"
import { gql } from "apollo-boost"

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControl from "@material-ui/core/FormControl";
import Button from "@material-ui/core/Button";
import Error from "../Shared/Error"


const UpdateLocation = ({location, classes, onLocationUpdate}) => {
  const [open, setOpen] = useState(false)
  const [horizontal, setHorizontal] = useState("")
  const [vertical, setVertical] = useState("")

  const handleSubmit = (event, updateLocation) => {
    event.preventDefault(); // prevents the page from reloading
    updateLocation({variables: { id: location.id, horizontal, vertical}})
    onLocationUpdate()
  }
  if (location.type === "init") {
    return (
      <React.Fragment>
        <IconButton edge="end" aria-label="edit" onClick={() => setOpen(true)}>
          <EditIcon />
        </IconButton>
        <Mutation 
          mutation={UPDATE_LOCATION_MUTATION}
          onCompleted={ data => {
            console.log(data)
            setOpen(false);
          }}
          refetchQueries={() => [ { query: GET_LOCATIONS_QUERY }]}
        >
          { (updateLocation, { error }) => {
            if (error) return <Error error={error}/>;

            return(
              <Dialog open={open} className={classes.dialog}>
                <form onSubmit={event => handleSubmit(event, updateLocation)}>
                  <DialogTitle>Editar inicio</DialogTitle>
                  <DialogContent>
                      <DialogContentText>
                          Agrega las coordenadas del punto a visitar (x, y)
                      </DialogContentText>
                      <FormControl fullWidth>
                          <input type="text" pattern="[0-9]*.[0-9]*"
                          label="x:"
                          placeholder="0"
                          onChange={ event => setHorizontal(event.target.value) }
                          className={classes.TextField}
                          />
                          <input type="text" pattern="[0-9]*.[0-9]*"
                          label="y:"
                          placeholder="0"
                          onChange={ event => setVertical(event.target.value) }
                          className={classes.TextField}
                          />
                      </FormControl>
                  </DialogContent>
                  <DialogActions>
                      <Button onClick={()=> setOpen(false) } className={classes.cancel}>Cancelar</Button>
                      <Button disabled={ !horizontal.trim() || !vertical.trim() } type="submit" className={classes.save}>Editar</Button>
                  </DialogActions>
                </form>
              </Dialog>
            );
          }}
      </Mutation>
    </React.Fragment>
    )
  }
  else {
    return (
      <div></div>
    )
  }

};


const UPDATE_LOCATION_MUTATION = gql`
  mutation($id: Int!, $horizontal: Float!, $vertical: Float!) {
    updateLocation(locationId:$id, horizontal: $horizontal, vertical: $vertical) {
      locationId
    }
  }
`

const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  dialog: {
    margin: "0 auto",
    maxWidth: 550
  },
  textField: {
    margin: theme.spacing.unit
  },
  cancel: {
    color: "red"
  },
  save: {
    color: "green"
  },
  button: {
    margin: theme.spacing.unit * 2
  },
  icon: {
    marginLeft: theme.spacing.unit
  },
  input: {
    display: "none"
  },
  fab: {
    position: "relative",
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
    zIndex: "200"
  }
});

export default  withStyles(styles)(UpdateLocation);
