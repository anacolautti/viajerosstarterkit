import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

import HomeIcon from '@material-ui/icons/Home';
import RoomIcon from '@material-ui/icons/Room';
import ListItemIcon from '@material-ui/core/ListItemIcon';

import CreateLocation from './CreateLocation'
import DeleteLocation from './DeleteLocation'
import UpdateLocation from './UpdateLocation'

import { Query } from "react-apollo"
import { gql } from "apollo-boost"


import Loading from "../Shared/Loading"
import Error from "../Shared/Error"

const LocationIcon = ({location}) => {
  if (location.type === "init") return <HomeIcon></HomeIcon>
  else return <RoomIcon></RoomIcon>
}

const LocationItems = ({classes, locations, onLocationUpdate}) => {
  return(
    <List>
    { locations.map(location => (
      <ListItem className={classes.root} key={location.id}>
        <ListItemIcon>
          <LocationIcon location={location} />
        </ListItemIcon>
        <ListItemText
          primaryTypographyProps={{
            variant: "subheading",
            color: "primary"
          }}
          primary={ location.type }
          secondary={`(${location.horizontal}, ${location.vertical})`}
          >
          
        </ListItemText>
        <ListItemSecondaryAction>
            <UpdateLocation location={location} onLocationUpdate={onLocationUpdate}></UpdateLocation>
            <DeleteLocation location={location} onLocationUpdate={onLocationUpdate}></DeleteLocation>
          </ListItemSecondaryAction>
        </ListItem>
      ))}
    </List>
  );
}

const LocationList = ({ classes, locations, onLocationUpdate}) => {
  return(
    <div style={{ display: "grid", placeItems: "center" }}>
      <Query query={GET_LOCATIONS_QUERY}>
        {({data, loading, error}) => {

            if (loading) return <Loading></Loading>
            if (error) return <Error error={error}></Error>

            locations = data.locations;
            return (
              <div style={{display:"flex", justifyContent:"space-evenly", paddingBottom: "100px"}}>
                <LocationItems classes={classes} locations={locations} onLocationUpdate={onLocationUpdate} />
              </div>
            )
        }}
      </Query>
      <CreateLocation onLocationUpdate={onLocationUpdate}></CreateLocation>
    </div>
  );

};

const styles = {
  root: {
    display: "flex",
    flexWrap: "wrap"
  },
  details: {
    alignItems: "center"
  },
  link: {
    color: "#424242",
    textDecoration: "none",
    "&:hover": {
      color: "black"
    }
  }
};

export const GET_LOCATIONS_QUERY = gql`
  {
    locations {
      id
      horizontal
      vertical
      type
    }
  }
`

export default withStyles(styles)(LocationList);
