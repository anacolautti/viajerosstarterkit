import React, { useState } from "react";
import { Mutation } from "react-apollo";
import { gql } from "apollo-boost";
import withStyles from "@material-ui/core/styles/withStyles";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControl from "@material-ui/core/FormControl";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";

import Error from "../Shared/Error"
import { GET_LOCATIONS_QUERY } from "../Location/LocationList"

const CreateLocation = ({ classes, onLocationUpdate }) => {
  const [open, setOpen] = useState(false)
  const [horizontal, setHorizontal] = useState("")
  const [vertical, setVertical] = useState("")

  const handleSubmit = (event, createLocation) => {
    event.preventDefault(); // prevents the page from reloading
    createLocation({variables: {horizontal, vertical}})
    onLocationUpdate()
  }
  
  return (
    <React.Fragment>
      <Button onClick={()=> setOpen(true) } variant="contained" color="primary" className={classes.fab}>
          <AddIcon></AddIcon>
          Agregar visita
      </Button>
      <Mutation 
        mutation={CREATE_LOCATION_MUTATION}
        onCompleted={data => {
          console.log({data});
          setOpen(false);
        }}
        refetchQueries={() => [ { query: GET_LOCATIONS_QUERY }]}
        >
        {(createLocation, { error }) => {
          if (error) return <Error error={error}/>;

          return(
            <Dialog open={open} className={classes.dialog}>
              <form onSubmit={event => handleSubmit(event, createLocation)}>
                <DialogTitle>Agregar visita</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Agrega las coordenadas del punto a visitar (x, y)
                    </DialogContentText>
                    <FormControl fullWidth>
                        <input type="text" pattern="[0-9]*.[0-9]*"
                        label="x:"
                        placeholder="0"
                        onChange={ event => setHorizontal(event.target.value) }
                        className={classes.TextField}
                        />
                        <input type="text" pattern="[0-9]*.[0-9]*"
                        label="y:"
                        placeholder="0"
                        onChange={ event => setVertical(event.target.value) }
                        className={classes.TextField}
                        />
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button onClick={()=> setOpen(false) } className={classes.cancel}>Cancelar</Button>
                    <Button disabled={ !horizontal.trim() || !vertical.trim() } type="submit" className={classes.save}>Agregar</Button>
                </DialogActions>
              </form>
            </Dialog>
          );
        }}

      </Mutation>
    </React.Fragment>
  );
};


const CREATE_LOCATION_MUTATION = gql`
  mutation($horizontal: Float!, $vertical: Float!) {
    createLocation(horizontal: $horizontal, vertical: $vertical)
    {
      location {
        id
        horizontal
        vertical
        type
      }
    }
  }
`



const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  dialog: {
    margin: "0 auto",
    maxWidth: 550
  },
  textField: {
    margin: theme.spacing.unit
  },
  cancel: {
    color: "red"
  },
  save: {
    color: "green"
  },
  button: {
    margin: theme.spacing.unit * 2
  },
  icon: {
    marginLeft: theme.spacing.unit
  },
  input: {
    display: "none"
  },
  fab: {
    position: "fixed",
    bottom: theme.spacing.unit * 2,
    left: theme.spacing.unit * 2,
    zIndex: "200"
  }
});

export default withStyles(styles)(CreateLocation);
