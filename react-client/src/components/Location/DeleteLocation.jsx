import React from "react";
import IconButton from "@material-ui/core/IconButton";
//import TrashIcon from "@material-ui/icons/DeleteForeverOutlined";
import DeleteIcon from '@material-ui/icons/Delete';
import { GET_LOCATIONS_QUERY } from "./LocationList"

import { Mutation } from "react-apollo"
import { gql } from "apollo-boost"

const DeleteLocation = ({location, onLocationUpdate}) => {
  return (
    <Mutation 
      mutation={DELETE_LOCATION_MUTATION}
      variables={{ id: location.id}}
      onCompleted={ data => {
        console.log(data)
        onLocationUpdate()
      }}
      refetchQueries={() => [ { query: GET_LOCATIONS_QUERY }]}

    >
      { deleteLocation => {

        if (location.type === "visit") {
          return (
            <IconButton edge="end" aria-label="delete" onClick={deleteLocation}>
              <DeleteIcon />
            </IconButton>
          )
        }
        else {
          return (
            <div></div>
          )
        }
      }
    }
    </Mutation>
  );
};


const DELETE_LOCATION_MUTATION = gql`
  mutation($id: Int!) {
    deleteLocation(locationId:$id) {
      locationId
    }
  }
`


export default DeleteLocation;
