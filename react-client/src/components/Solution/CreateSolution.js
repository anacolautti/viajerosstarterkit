import React, { useState } from "react";
import { Mutation } from "react-apollo";
import { gql } from "apollo-boost";
import withStyles from "@material-ui/core/styles/withStyles";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import AddIcon from "@material-ui/icons/Add";

import Error from "../Shared/Error"

const CreateSolution = ({ classes }) => {
  const [open, setOpen] = useState(false)
  const [source, setSource] = useState("")

  const handleSubmit = (event, createSolution) => {
    //event.preventDefault(); // prevents the page from reloading
    createSolution({variables: {source}})
  }

  return (
    <React.Fragment>
      <Button onClick={()=> setOpen(true) } variant="contained" color="primary" className={classes.fab}>
          <AddIcon></AddIcon>
          Crear solución
      </Button>
      <Mutation 
        mutation={CREATE_SOLUTION_MUTATION}
        onCompleted={data => {
          console.log({data});
          setOpen(false);
        }}
        >
        {(createSolution, {loading, error }) => {
          if (error) return <Error error={error}/>;

          return(
            <Dialog open={open} className={classes.dialog}>
              <form onSubmit={event => handleSubmit(event, createSolution)}>
                <DialogTitle>Generar solución</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Selecciona el tipo de solución
                    </DialogContentText>
                    <FormControl fullWidth>
                        <input type="select"
                        onChange={ event => setSource(event.target.value) }
                        className={classes.TextField}
                        >
                          <option value="random">RANDOM</option>
                        </input>
                        
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button onClick={()=> setOpen(false) } className={classes.cancel}>Cancelar</Button>
                    <Button disabled={ !source.trim() } type="submit" className={classes.save}>Generar</Button>
                </DialogActions>
              </form>
            </Dialog>
          );
        }}

      </Mutation>
    </React.Fragment>
  );
};


const CREATE_SOLUTION_MUTATION = gql`
  mutation($source: String!) {
    createSolution(source: $source) {
      solution {
        id
        iteration
        source
        origins {
          id
          type
        }
        visits {
          id
          type
        }
      }
    }
  }
`



const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  dialog: {
    margin: "0 auto",
    maxWidth: 550
  },
  textField: {
    margin: theme.spacing.unit
  },
  cancel: {
    color: "red"
  },
  save: {
    color: "green"
  },
  button: {
    margin: theme.spacing.unit * 2
  },
  icon: {
    marginLeft: theme.spacing.unit
  },
  input: {
    display: "none"
  },
  fab: {
    position: "fixed",
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
    zIndex: "200"
  }
});

export default withStyles(styles)(CreateSolution);
