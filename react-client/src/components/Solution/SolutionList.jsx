import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';


import ListItemIcon from '@material-ui/core/ListItemIcon';


import Loading from "../Shared/Loading"
import Error from "../Shared/Error"


const SolutionItems = ({classes, solutions, onListClick, selectedSolutionId}) => {
  return(
    <List>
      { solutions.map((solution, index) => {
        console.log(solution.id, selectedSolutionId);
        return(
          <div style={{ border: `${(solution.id === selectedSolutionId) ? "4px solid #ff5b5b" : ""}`}} key={solution.id} >
            <ListItem 
              className={classes.root} 
              key={solution.id} 
              >
              <ListItemText
                primaryTypographyProps={{
                  variant: "subheading",
                  color: "primary"
                }}
                primary={`[${solution.cost}] ${solution.source}`}
                secondary={`Iteración (${ solution.generation.iteration }) - id ${solution.id}`}
                onClick={()=>{ onListClick(solution.id)}}
                >
                
              </ListItemText>
            </ListItem>
          </div>
        );
    })}
    </List>
  );
}

const SolutionList = ({ classes, solutions, onListClick, selectedSolutionId}) => {
//  console.log(solutions);
  return(
    <div>
      <SolutionItems  classes={classes} solutions={solutions} onListClick={onListClick} selectedSolutionId={selectedSolutionId}></SolutionItems>
    </div>
  );

};

const styles = {
  root: {
    display: "flex",
    flexWrap: "wrap"
  },
  details: {
    alignItems: "center"
  },
  link: {
    color: "#424242",
    textDecoration: "none",
    "&:hover": {
      color: "black"
    }
  }
};


export default withStyles(styles)(SolutionList);
