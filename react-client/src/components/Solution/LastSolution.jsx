import React, { useState } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';

import HomeIcon from '@material-ui/icons/Home';
import RoomIcon from '@material-ui/icons/Room';

import ListItemIcon from '@material-ui/core/ListItemIcon';

import CreateSolution from './CreateSolution'

import Typography from "@material-ui/core/Typography";
import color from "@material-ui/core/colors/amber";

import Loading from "../Shared/Loading"
import Error from "../Shared/Error"
import { Query } from "react-apollo"
import { gql } from "apollo-boost"
import Map2d from "../Map2d/Map2d"


const SolutionIcon = ({solution}) => {
  if (solution.iteration == 0) return <HomeIcon></HomeIcon>
  else return <RoomIcon></RoomIcon>
}

function buildWinner ( solutions ) {
  const winner = [];

  const best_solution = solutions[0];

  const solution_id = best_solution;

  console.log("ID!",solution_id)

  return winner;
}


const LastSolution = ({ classes }) => {
  const [solutions, setSolutions] = useState([])
  const [origins, setOrigins] = useState([])
  const [visits, setVisits] = useState([])

  const [lastsolution, setLastSolution] = useState([])
  


  return(
    <div>
      <Query query={GET_SOLUTIONS_QUERY}>
        {({data, loading, error}) => {

            if (loading) return <Loading></Loading>
            if (error) return <Error error={error}></Error>

            /*setSolutions(data.bestSolutions);
            


            console.log("Solutions", solutions)
            

            buildWinner ( solutions )*/

            return ("hola")
        }}
      </Query>
      <Map2d style={{flexGrow:"1"}} solution={solutions}></Map2d>
      <CreateSolution></CreateSolution>

    </div>
  );

};

/*      <List>
        { solutions.map(solution => (
          <ListItem className={classes.root} key={solution.id}>
            <ListItemIcon>
              <SolutionIcon solution={solution} />
            </ListItemIcon>
            <ListItemText
              primaryTypographyProps={{
                variant: "subheading",
                color: "primary"
              }}
              primary={ solution.source }
              secondary={`(${solution.origins}, ${solution.visits})`}
              >
              
            </ListItemText>
          </ListItem>
        ))}
      </List>*/
const GET_SOLUTIONS_QUERY = gql`
  {
    bestSolutions: bestSolution {
      id
      cost
      generation {
        iteration
        runIteration {
          id
          current
        }
      }
      originlocationsSet {
        routeID
        location {
          horizontal
          vertical
        }
      }
      visitlocationsSet {
        routeID
        index
        location {
          horizontal
          vertical
        }
      }
    }
  }
`

/*
    origins: originLocations {
      routeID
      location {
        id
      }
      solution {
        id
      }
    }
    visits: visitLocations {
      routeID
      index
      location {
        id
      }
      solution {
        id
      }
    }
*/


const styles = {
  root: {
    display: "flex",
    flexWrap: "wrap"
  },
  details: {
    alignItems: "center"
  },
  link: {
    color: "#424242",
    textDecoration: "none",
    "&:hover": {
      color: "black"
    }
  }
};

export default withStyles(styles)(LastSolution);
