import React, { Component } from 'react';
import { Stage } from '@inlet/react-pixi';
import Grid from '../Grid/Grid.js';
import Locations from './Locations'
import ColoredPath from '../Map2d/ColoredPath';


class Map2d extends Component {
  constructor (props) {
    super(props);
    this.state = {
      cx: 100,
      cy: 200,
      timer: null,
      width: 502,
      height: 502,
      scale: 50
    }
  }

  componentDidMount() {
    const timer = setInterval( () => {
      const cy = (Math.random() * 500).toFixed(0);
      const cx = (Math.random() * 500).toFixed(0);
      this.setState ( {...this.state, cx: cx, cy: cy});
      //console.log ('New coordinates: ', {cx, cy});

    }, 500);
    this.setState( { ...this.state, timer: timer});
  }

  componentWillUnmount() {
    if (this.state.timer !== null) clearInterval(this.state.timer);
  }

  render () {
    //console.log(this.props);
    const {origins, visits, routes} = this.props
    //debugger;
    return (
      <div className="Map" style={{ display:"grid", placeItems:"center" }}>
        <Stage 
          width={this.state.width} 
          height={this.state.height} 
          options={{ transparent: true }}
        >
          <Grid 
            width={this.state.width - 2} 
            height={this.state.height -2} 
            scale={this.state.scale}/>
          { (routes.length !== 0) && (this.props.draw) ? <ColoredPath scale={this.state.scale} routes={routes} /> : null }
          <Locations origins={origins} visits={visits} scale={this.state.scale} routes={routes}/>

        </Stage>
      </div>
    );
  }
}
/*
          <Sprite image="https://s3-us-west-2.amazonaws.com/s.cdpn.io/693612/IaUrttj.png" x={this.state.cx} y={this.state.cy} />          
          

*/

export default Map2d;
