import React, {useState} from 'react'
import { Container, Sprite, Text } from '@inlet/react-pixi';



const Locations = ({origins, visits, scale, routes}) => {
    const [showHelp, setShowHelp] = useState(false)
    const [horizontal, setHorizontal] = useState(0)
    const [vertical, setVertical] = useState(0)
    const [value, setValue] = useState(0)

    origins.forEach(element => {
        element["index"] = 0
    });
    
    visits.forEach(element =>{
        routes.forEach(path => {
            path.forEach( (step, index) => {
                if (step.id === element.id) {
                    element["index"] = index
                }
            })
        })
    });

    return (
        <Container>
            { /*routes.map( (path) => {
                return path.map( (element, i) => {
                    console.log("LELEMENT!", element)
                    let icon = ""
                    if (i === 0) {
                        icon = "https://upload.wikimedia.org/wikipedia/commons/b/bc/House_image_icon.png"
                    } else {
                        icon = "https://upload.wikimedia.org/wikipedia/commons/9/9e/Pin-location.png"
                    }
                    if (i === path.length-1) {
                        return
                    }
                    return (
                        <Sprite 
                            key={i}
                            x={scale*(element.horizontal)}
                            y={scale*(element.vertical)}
                            anchor={0.5}
                            scale={0.2}
                            interactive={true}
                            mouseover={(mouseData)=>{
                                console.log("IN", mouseData)
                                setShowHelp(true)
                                setValue(i)
                                setHorizontal(scale*(element.horizontal))
                                setVertical(scale*(element.vertical))
                            }}
                            mouseout = {(mouseData) => {
                                setShowHelp(false)

                                console.log("OUT", mouseData)
                            }}
                            image={icon}
                        />
                    );
                });
            })*/}
            { origins.map( (element, i) => {
                return (
                    <Sprite 
                        key={i}
                        x={scale*(element.horizontal)}
                        y={scale*(element.vertical)}
                        anchor={0.5}
                        scale={0.2}
                        interactive={true}
                        mouseover={(mouseData)=>{
                            console.log("IN", mouseData)
                            setShowHelp(true)
                            setValue(element.index)
                            setHorizontal(scale*(element.horizontal))
                            setVertical(scale*(element.vertical))
                        }}
                        mouseout = {(mouseData) => {
                            setShowHelp(false)

                            console.log("OUT", mouseData)
                        }}
                        image="https://upload.wikimedia.org/wikipedia/commons/b/bc/House_image_icon.png"
                    />
                );
            })
            }
            { visits.map( (element, i) => {
                return (
                    <Sprite 
                        key={i} 
                        x={scale*element.horizontal} 
                        y={scale*element.vertical} 
                        anchor={[0.5,1]} 
                        scale={0.15}
                        interactive={true}

                        mouseover={(mouseData)=>{
                            console.log("IN", mouseData)
                            setShowHelp(true)

                            setValue(element.index)
                            setHorizontal(scale*(element.horizontal))
                            setVertical(scale*(element.vertical))
                        }}
                        mouseout = {(mouseData) => {
                            setShowHelp(false)

                            console.log("OUT", mouseData)
                        }}
                        image="https://upload.wikimedia.org/wikipedia/commons/9/9e/Pin-location.png"
                    />
                );
            })}
            { showHelp && <Text text={value} x={horizontal} y={vertical} style={{padding: 20, fill:"0x000000", strokeThickness:2}}/> }
        </Container>

    );
};


export default Locations;