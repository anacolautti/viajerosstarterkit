import React from 'react';
import { Graphics } from '@inlet/react-pixi';


// Tooltip con coordenadas de locations on hover?
const coloredPath = (g, dh, dv, locations, color=0xFF0000, lineWidth=4) => {
  if (locations && locations.length === 0) {
    return;
  }
  g.lineStyle(lineWidth, color, 1);
  let previous = locations[0];
  locations.map(
  (location)=>{
    g.moveTo(dh*previous.horizontal, dv*previous.vertical);
    g.lineTo(dh*previous.horizontal, dv*location.vertical);
    g.moveTo(dh*previous.horizontal, dv*location.vertical);
    g.lineTo(dh*location.horizontal, dv*location.vertical);
    previous = location;
    return null
  });

  
};

const ColoredPath = (props) => {
  const {scale, routes} = props;
  return (
      <Graphics
        draw={g => {        
        const dh = scale;
        const dv = scale;

        g.clear();

        coloredPath(g, dh, dv, routes[0], 0x0abb00, 4); // green
        coloredPath(g, dh, dv, routes[1], 0x0f21a9, 4); // blue

        return null

      }}
    />
  )
}

export default ColoredPath;